function [  ] = setInArDict( dataEntry, value )
% SETINARDICT will set the value of a specified entry in the Ar
% Drone data dictionary.

    Simulink.data.dictionary.open('ArDroneDataDictionary.sldd').getSection('Design Data').getEntry(dataEntry).setValue(value);
end

