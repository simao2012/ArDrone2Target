function SSHDownload(varargin)
%SSHDOWNLOAD is used to upload the model compiled for the ARM AR Drone target to the AR Drone 2.0 and execute it 

%% Retrieve the IP
fileName = char(varargin(2));
fileName = fileName(4:end); % remove ../
modelName = fileName(1: end - 4); %remove .elf

hCs = getActiveConfigSet(modelName);
data = hCs.get_param('CoderTargetData');
IP = data.IP;

disp(['Setting up FTP Connection with the AR Drone at IP adress: ' IP ':5551']);
connectionStatus = 0;
stopUpload = false;
while connectionStatus == 0
    try
        droneFtp = ftp([IP ':5551'],'root','root');
        connectionStatus = 1;
    catch
          dialogOutput = questdlg('Could not upload the executable to the AR Drone 2.0. Make sure that you are connected to the AR Drone 2.0 over WIFI before pressing Retry.','Cannot connect to AR Drone 2.0','Retry','Abort','Retry');
          if strcmp(dialogOutput,'Abort')
              stopUpload = true;
              disp(['Upload canceled by the user']);
              break;
          end
%        msgbox('Could not upload the executable to the AR Drone 2.0.Make sure that you are connected to the AR Drone 2.0 over WIFI before pressing OK.','Cannot connect to AR Drone 2.0','modal') 
    end
end
if ~stopUpload
    disp(['Uploading ' fileName ' to the AR Drone']);
    %Delete any older versions of the program on the drone because mput does
    %not overwrite files
    try
        delete(droneFtp,fileName);
    catch
    end

    % copy the executable to the drone
    mput(droneFtp,which(fileName));

    disp('Opening TPCIP connection with the AR Drone');
    droneTcpip = tcpip(IP,23);
    fopen(droneTcpip);


    disp(['Killing any instances of Program.elf or previously running ' fileName]);
    query(droneTcpip,'killall -9 program.elf.respawner.sh');
    query(droneTcpip,'killall -9 program.elf');
    query(droneTcpip,['killall -9 ' fileName]);

    disp('Starting the program on the AR Drone');
    query(droneTcpip,['chmod 777 /update/' fileName]);

    outputFileName = [fileName(1:end-4) '.txt']; %create a file to save the terminal output to
    query(droneTcpip,['./update/' fileName ' -w > /update/' outputFileName]);
end
end


