#ifndef _ARCLOCK_H
#define _ARCLOCK_H

#ifndef MATLAB_MEX_FILE
#endif //MATLAB_MEX_FILE

void ArClockInit(void);
void ArClockClose(void);
void ArClockStep(double *time);
double ArClock(void);

#endif