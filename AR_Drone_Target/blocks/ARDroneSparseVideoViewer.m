function ARDroneSparseVideoViewer(block)
% Level-2 MATLAB file S-Function for unit delay demo.
%   Copyright 1990-2009 The MathWorks, Inc.

  setup(block);
  
%endfunction

function setup(block)
   
  %% Register number of input and output ports
  block.NumInputPorts  = 3;
  block.NumOutputPorts = 0;
  block.NumDialogPrms  = 2;

  %% Setup functional port properties to dynamically
  %% inherited.
  block.SetPreCompInpPortInfoToDynamic;
  
  %% Set block sample time to inhereted
  block.SampleTimes = [-1 0];
  
  %% Set the block simStateCompliance to default (i.e., same as a built-in block)
  block.SimStateCompliance = 'DefaultSimState';

  %% Register methods
  block.RegBlockMethod('InitializeConditions',    @InitConditions);  
  block.RegBlockMethod('Update',                  @Update);  
  
  %% Set block as a viewer
  block.SetSimViewingDevice(true);
  
%endfunction

function InitConditions(block)

  
%endfunction

function Update(block)

%% find the video resolution from the input length
xIndexes = block.InputPort(1).Data;     %uint16
yIndexes = block.InputPort(2).Data;     %uint16
intensities = block.InputPort(3).Data;  %uint8

width = block.DialogPrm(1).Data;  % 1280
height = block.DialogPrm(2).Data; % 720

C = uint8(zeros(height,width)); % probably need to flip both axises for image

%% Apply intensities
for ii = 1 : length(xIndexes)
    if (xIndexes(ii) > 0 && xIndexes(ii) <= width && ...
            yIndexes(ii) > 0 && yIndexes(ii) <= height)
        C(yIndexes(ii),xIndexes(ii)) = intensities(ii);
    end
end

%% Create and show the RGB image (by using RGB the intensities are grayscaled)
RGB = uint8(zeros(height,width,3));
for ii = 1 : 3
    RGB(:,:,ii) = C;
end
image(RGB);

%endfunction

